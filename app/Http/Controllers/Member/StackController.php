<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Models\Stack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StackController extends Controller
{

    public function show($id)
    {
        $user_id = Auth::user()->id;
        $stack = Stack::find($id);
        return view('member.stack_details.index')
            ->with('stack', $stack);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'user_id' => 'required'
        ]);

        $stack = new Stack();

        $stack->name = $request->name;
        $stack->created_by = $request->user_id;

        $stack->save();
        return redirect()->back();
    }

}
