<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Card extends Model
{
    use SoftDeletes;

    protected $attributes = [
        'total_read' => 0,
        'is_public' => false
    ];

    public function stacks()
    {
        return $this->belongsToMany('App\Models\Stack');
    }
}
