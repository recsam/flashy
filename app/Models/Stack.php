<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stack extends Model
{
    public function cards()
    {
        return $this->belongsToMany('App\Models\Card');
    }
}
