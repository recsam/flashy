<?php

namespace App\View\Components\Shared;

use App\Models\Stack;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Component;

class TagAdd extends Component
{

    public $stacks;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        $user_id = Auth::user()->id;
        $stacks = Stack::where('created_by', $user_id)->get();
        $this->stacks = $stacks;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.shared.tag-add');
    }
}
