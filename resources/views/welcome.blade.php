<x-layouts.app>
<div class="center">
<h1>Create Flashcards, memorize more</h1>
<div class="mt-4 d-flex flex-column justify-content-center">
    <a href="{{ route('register') }}" class="btn btn-primary mb-2">Get Started</a>
    <a href="{{ route('login') }}" class="btn btn-secondary">I already have an account</a>
</div>
</div>
</x-layouts.app>
