<x-layouts.auth-app>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-bg p-2 py-4 p-sm-5 shadow">
                <h1 class="mb-3">{{ __('Login') }}</h1>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <label for="inputEmail">Email address</label>
                        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="inputEmail" placeholder="Enter email" value="{{ old('email') }}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputPassword">Password</label>
                        <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" id="inputPassword" placeholder="Password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group m-n3">
                        @if (Route::has('password.request'))
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Forgot Your Password?') }}
                        </a>
                        @endif
                    </div>
                    <div class="d-flex flex-column flex-sm-row justify-content-between mt-4">
                        <div class="d-flex">
                            <p class="mr-1">Not yet have an account?</p>
                            <a href="{{ route('register') }}">Create an account</a>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</x-layouts.auth-app>
