<x-layouts.member-app page-key="STACK_DETAILS">
    <x-slot name="current_stack">
        {{ $stack->id }}
    </x-slot>

    <h1>{{ $stack->name }}</h1>
    <x-shared.card-list :cards="$stack->cards" />

</x-layouts.member-app>
