<div class="modal fade" id="createStackModal" tabindex="-1">
    <div class="modal-dialog">
        <form method="POST" action="{{ route('stacks.store') }}" class="modal-content modal-body">
            @csrf
            <div class="d-flex justify-content-between mb-2">
                <h5>Create Stack</h5>
                <button type="button" class="close mt-n2" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <input type="number" class="d-none" name="user_id" value="{{ Auth::user()->id }}">
            <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Stack name ...">
            </div>
            <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary ml-2">Save</button>
            </div>
        </form>
    </div>
</div>
