<x-shared.card :href="route('cards.create')" class="add-card text-primary">
    <i class="fa fa-2x fa-plus"></i>
</x-shared.card>
