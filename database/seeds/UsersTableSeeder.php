<?php

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        for ($i = 1; $i <= 5; $i++) {
            User::create([
                'username' => 'admin' . $i,
                'email' => 'admin' . $i .'@admin.com',
                'password' => Hash::make('12345678'),
                'role' => 'admin'
            ]);
        }
        for ($i = 1; $i <= 10; $i++) {
            User::create([
                'username' => 'member' . $i,
                'email' => 'member'. $i .'@member.com',
                'password' => Hash::make('12345678'),
                'role' => 'member'
            ]);
        }
        Model::reguard();
    }
}
